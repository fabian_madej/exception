package fabian.task;

import fabian.task.classes.InvalidNumberException;
import fabian.task.classes.MathUtils;

public class Main {
    public static void main(String[] args) throws InvalidNumberException {
        try {
            System.out.println(MathUtils.factorial(-1));
        }catch (InvalidNumberException ex){
            System.out.println(ex.getMessage());
        }
        try {
            System.out.println(MathUtils.factorial(17));
        }catch (InvalidNumberException ex){
            System.out.println(ex.getMessage());
        }
        try {
            System.out.println(MathUtils.factorial(5));
        }catch (InvalidNumberException ex){
            System.out.println(ex.getMessage());
        }
    }
}
