package fabian.task.classes;

public class MathUtils {
    public static int factorial(int number) throws InvalidNumberException {
        if(number<0){
            throw new InvalidNumberException("Lower than 0!!!");
        }else if(number>12){
            throw new InvalidNumberException("Bigger than 12!!!");
        }else {
            if(number== 0 || number==1){
                return 1;
            }else {
                return factorial(number-1)*number;
            }
        }
    }
}
